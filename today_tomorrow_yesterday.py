import re
import sys
from datetime import datetime, timedelta

def today(days=0, fmt='%Y-%m-%d', string=True):
    today = datetime.now() + timedelta(days)
    if string:
        return today.strftime(fmt)
    return today   # type: datetime.datetime

def yesterday():
    return today(days=-1)

def tomorrow():
    return today(days=1)

print(f'Number of arguments: {len(sys.argv)}')
print(f'Argument List: {str(sys.argv)}\n')

for x in range(1, len(sys.argv)):
    tokens = 0
    print(f'Argument: {sys.argv[x]}')
    pattern = r'^(?:today([+-]\d+)?)$|^tomorrow$|^yesterday$'

    if re.findall(pattern, sys.argv[x]):
        tokens = sys.argv[x].replace('-',' - ').replace('+',' + ').split()

        token = tokens.pop(0)   # token: today|tomorrow|yesterday
        if token == 'today':
            if len(tokens) == 0:
                datum = datetime.now()
                print(f'   date: {datum}\n')
            else:
                dagen = int(f'{tokens.pop(0)}{tokens.pop(0)}')
                print(f'   date: {today(dagen)}\n')
        elif token == 'tomorrow':
            print(f'   date: {today(+1)}\n')
        else:   # token == 'yesterday'
            print(f'   date: {today(-1)}\n')
    else:
        print(f'   Argument: {sys.argv[x]}')
        print(f'   Error: pattern => "^(?:today([+-]\d+)?)$|^tomorrow$|^yesterday$"\n')