# utils

```
$ python regex.py todag today+77 today-2 today today+ tomorrow yesterday

Number of arguments: 8
Argument List: ['regex.py', 'todag', 'today+77', 'today-2', 'today', 'today+', 'tomorrow', 'yesterday']

Argument: todag
   Argument: todag
   Error: pattern => "^(?:today([+-]\d+)?)$|^tomorrow$|^yesterday$"

Argument: today+77
   date: 2024-03-20

Argument: today-2
   date: 2024-01-01

Argument: today
   date: 2024-01-03 23:26:20.430356

Argument: today+
   Argument: today+
   Error: pattern => "^(?:today([+-]\d+)?)$|^tomorrow$|^yesterday$"

Argument: tomorrow
   date: 2024-01-04

Argument: yesterday
   date: 2024-01-02
```